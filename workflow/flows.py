from viewflow import flow
from viewflow.base import this, Flow

from viewflow.flow.views import CreateProcessView, UpdateProcessView

from . import models, forms


class CompraFlow(Flow):
    """
    Proceso de compra
    """
    process_class = models.Compra
    process_title = "Compra"
    process_description = "Proceso de solicitud de compra"

    start = flow.Start(CreateProcessView, fields="__all__",
                       task_title="Nueva compra").Permission(
        auto_create=True).Next(this.end)

    end = flow.End()
