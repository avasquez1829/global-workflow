from django.views.generic import TemplateView
from viewflow.flow.views import StartFlowMixin, FlowMixin
from .forms import CompraForm
from django.shortcuts import redirect
from viewflow.flow.views.utils import get_next_task_url


class CompraListView(StartFlowMixin):
    template_name = 'workflow/compras_list.html'

    form = CompraForm

    def done(self, form, **kwargs):
        compras = form.save()

        self.activation.process.compras = compras
        self.activation.done()

        return redirect(get_next_task_url(self.request, self.activation.process))