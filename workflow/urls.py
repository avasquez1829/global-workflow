from django.urls import path, include
from django.conf.urls import url
from viewflow.flow.viewset import FlowViewSet

from .flows import CompraFlow

compra_urls = FlowViewSet(CompraFlow).urls

app_name = 'workflow'

urlpatterns = [
    url(r'^compra/', include((compra_urls, 'compra'))),
]
