from django.forms import ModelForm
from material import Layout, Row, Span2

from .models import Compra


class CompraForm(ModelForm):
    layout = Layout(
            Row("company", "branch"),
            Row("order_number", "price"),
            Row("requester", "request_date"),
            Row("description")

    )

    class Meta:
        model = Compra
        fields = '__all__'
