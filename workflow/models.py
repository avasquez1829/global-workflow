from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _
from viewflow.models import Process, Task

from bpmn_project.settings import AUTH_USER_MODEL


class Compra(Process):
    company = models.CharField(_("Company"), max_length=100)
    branch = models.CharField(_("Branch"), max_length=50)
    requester = models.CharField(_("Requester"), max_length=100)
    request_date = models.DateTimeField()
    description = models.CharField(_("Description"), max_length=255)
    order_number = models.CharField(_("Order number"), max_length=20)
    price = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Compra"
        verbose_name_plural = "Compras"

    def __str__(self):
        return " Cotización #"


# class CompraProcess(Process):
#     compra = models.ForeignKey(Compra, on_delete=models.CASCADE, blank=True,
#                                null=True)
#
#     class Meta:
#         verbose_name_plural = "Listado de compras"
        # permissions = [("can_start_request", "Can Start buy request")]
#
#
# class CompraTask(Task):
#     class Meta:
#         proxy = True
#
#
# class Flow(models.Model):
#     """Model definition for Flow."""
#
#     code = models.CharField(_("code"), max_length=40)
#     description = models.CharField(_("description"), max_length=255)
#     version = models.CharField(_("version"), max_length=50)
#     created_at = models.DateTimeField(_("created at"), auto_now_add=True)
#
#     class Meta:
#         """Meta definition for Flow."""
#
#         verbose_name = "Flow"
#         verbose_name_plural = "Flows"
#
#     def __str__(self):
#         """Unicode representation of Flow."""
#         return f"FLow-{self.code}: {self.description[:50]}..."
#
#
# class Task(models.Model):
#     """Model definition for Task."""
#
#     flow = models.ManyToManyField(Flow, related_name="flows")
#     requester = models.ForeignKey(
#             settings.AUTH_USER_MODEL,
#             on_delete=models.CASCADE,
#             related_name="task_requester",
#     )
#     started = models.DateTimeField(_("started"), auto_now_add=True)
#     finished = models.DateTimeField(_("finished"), auto_now=True)
#
#
# class ServiceLevelAgreement(models.Model):
#     """Model definition for ServiceLevelAgreement."""
#
#     name = models.CharField(_("name"), max_length=40)
#     unity = models.CharField(_("unity"), max_length=40)
#     value = models.PositiveIntegerField()
#
#     class Meta:
#         """Meta definition for ServiceLevelAgreement."""
#
#         verbose_name = "ServiceLevelAgreement"
#         verbose_name_plural = "ServiceLevelAgreements"
#
#     def __str__(self):
#         """Unicode representation of ServiceLevelAgreement."""
#         return self.name
#
#
# class Workflow(models.Model):
#     """Model definition for Workflow."""
#
#     task = models.ManyToManyField(Task)
#     requester = models.ForeignKey(
#             settings.AUTH_USER_MODEL,
#             on_delete=models.CASCADE,
#             related_name="requester",
#     )
#     assigned_to = models.ForeignKey(
#             settings.AUTH_USER_MODEL,
#             on_delete=models.CASCADE,
#             related_name="assigned",
#     )
#     status = models.CharField(_("status"), max_length=50)
#     sla = models.ManyToManyField(ServiceLevelAgreement, )
#     step = models.CharField(_("step"), max_length=50)
#     expiration_date = models.DateTimeField(_("Expiration Date"))
#     started = models.DateTimeField(
#             _("started"), auto_now_add=True
#     )
#     finished = models.DateTimeField(_("finished"), auto_now=True)
#
#     class Meta:
#         """Meta definition for Workflow."""
#
#         verbose_name = "Workflow"
#         verbose_name_plural = "Workflows"
#
#     def __str__(self):
#         """Unicode representation of Workflow."""
#         return f"{self.pk}"
