from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView

urlpatterns = [
                  path("",
                       RedirectView.as_view(url="/workflow/compra", permanent=False)),
                  path("workflow/",
                       include("workflow.urls", namespace='workflow')),
                  path("accounts/", include("core.urls", namespace='core')),
                  path("accounts/", include("django.contrib.auth.urls")),
                  path("api-auth/", include("rest_framework.urls")),
                  path("ee3040565933ca2fecc69739fbf053c6/", admin.site.urls),
                  path("rosetta/", include("rosetta.urls")),
              ] + static(settings.STATIC_URL,
                         document_root=settings.STATIC_ROOT)
