# Pull base image
FROM python:3.7.6-alpine

# Label image
LABEL mantainer="Global Business IT Corp"
LABEL developer="Alexander Vásquez"

## Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
# WORKDIR /usr/src/app
WORKDIR /code
RUN pwd

# Adding mandatory packages to docker
RUN apk update && apk add --no-cache \
    postgresql zlib jpeg \
    && apk add --no-cache --virtual build-deps \
    gcc python3-dev musl-dev postgresql-dev \
    zlib-dev jpeg-dev

COPY Pipfile Pipfile.lock /code/

RUN pip install --upgrade pip \
    && pip install pipenv \
    && pipenv install --system \
    && apk --purge del build-deps

## Copy project
COPY . /code/

ENTRYPOINT [ "/code/entrypoint.sh" ]
