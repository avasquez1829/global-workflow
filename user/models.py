from django.conf import settings
from django.db import models


class UserProfile(models.Model):
    user_id = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    phone = models.CharField(max_length=40, blank=True)
    profile_pic = models.ImageField(upload_to="profile_pics", blank=True)
    country = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return str(self.user_id)
