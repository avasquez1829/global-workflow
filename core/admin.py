from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUSerAdmin
from django.utils.translation import gettext_lazy as _

from user.models import UserProfile

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import User


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    fieldsets = ((None, {"fields": ("phone", "country", "profile_pic",),}),)
    # exclude = ("user_id",)


@admin.register(User)
class UserAdmin(DjangoUSerAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    inlines = [
        UserProfileInline,
    ]
    list_display = (
        "email",
        "is_active",
        "is_superuser",
        "last_login",
    )
    list_filter = (
        "email",
        "is_staff",
        "is_active",
    )
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_staff",
                    "is_active",
                    "is_admin",
                    "is_superuser",
                    "user_permissions",
                )
            },
        ),
        (_("Groups"), {"fields": ("groups",)}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "password1",
                    "password2",
                    "is_superuser",
                    "is_active",
                ),
            },
        ),
    )
    search_fields = ("email", "firstname", "lastname")
    ordering = ("email",)
