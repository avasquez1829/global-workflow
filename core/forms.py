from django import forms
from django.contrib.auth import (
    authenticate,
    get_user_model,
    password_validation,
)
from django.contrib.auth.forms import (
    AuthenticationForm,
    PasswordResetForm,
    UserChangeForm,
    UserCreationForm,
    UsernameField,
)
from django.utils.text import capfirst
from django.utils.translation import gettext_lazy as _

from .models import User

UserModel = get_user_model()


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = User
        fields = ("email",)


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ("email",)


class CustomAuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """

    username = forms.CharField(
        widget=forms.EmailInput(attrs={"autofocus": True, 'id': 'username'}))
    password = forms.CharField(
            label=_("Password"),
            strip=False,
            widget=forms.PasswordInput(
                attrs={"autocomplete": "current-password"}),
    )

    error_messages = {
        "invalid_login": _("Please enter a correct %(username)s and password"),
        "inactive": _("This account is inactive."),
        "invalid_email": _("Wrong credentials"),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)

        # Set the max length and label for the "username" field.
        self.username_field = UserModel._meta.get_field(
                UserModel.USERNAME_FIELD
        )
        username_max_length = self.username_field.max_length or 254
        self.fields["username"].max_length = username_max_length
        self.fields["username"].widget.attrs["maxlength"] = username_max_length
        if self.fields["username"].label is None:
            self.fields["username"].label = capfirst(
                    self.username_field.verbose_name
            )

    def clean(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        if username is not None and password:
            try:
                user = UserModel.objects.get(email=username)
            except:
                raise forms.ValidationError(
                        self.error_messages["invalid_email"],
                        code="invalid_email",
                )

            # if user is None:
            self.user_cache = authenticate(
                    self.request, username=username, password=password
            )
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                    self.error_messages["inactive"], code="inactive",
            )

    def get_user(self):
        return self.user_cache

    def get_invalid_login_error(self):
        return forms.ValidationError(
                self.error_messages["invalid_login"],
                code="invalid_login",
                params={"username": self.username_field.verbose_name},
        )


class CustomPasswordResetForm(PasswordResetForm):
    error_messages = {
        "invalid_email": _("Please enter your email address correctly."),
    }

    def clean(self):
        username = self.cleaned_data.get("username")

        if username is not None and password:
            try:
                user = UserModel.objects.get(email=username)
            except:
                raise forms.ValidationError(
                        self.error_messages["invalid_email"],
                        code="invalid_email",
                )
        return self.cleaned_data
