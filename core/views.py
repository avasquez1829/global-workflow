from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.views import LoginView, PasswordResetView
from django.shortcuts import render
from django.views import generic
from .forms import CustomAuthenticationForm, CustomPasswordResetForm


class Login(LoginView):
    authentication_form = CustomAuthenticationForm
    # form_class = CustomAuthenticationForm
    # template_name = "registration/login.html"
    template_name = "registration/login.html"


class PasswordReset(PasswordResetView):
    form_class = CustomPasswordResetForm


class SettingsView(generic.TemplateView):
    template_name = 'core/manage account.html'
