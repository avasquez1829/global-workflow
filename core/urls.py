from django.urls import include, path
from rest_framework import routers
from django.contrib.auth.views import LogoutView
from core.api.views import UserViewSet
from django.views import generic

from . import views

router = routers.DefaultRouter()
router.register("", UserViewSet)

app_name = 'core'

urlpatterns = [
    path("login/", views.Login.as_view(), name="login"),
    path("password_reset/", views.PasswordReset.as_view(),
         name="password_reset"),
    path('logout/', LogoutView.as_view(), name="logout"),
    path("users/", include(router.urls)),
    path("manage_account/", generic.TemplateView.as_view(
        template_name="core/manage account.html"), name="manage_account"),
    path("help/", generic.TemplateView.as_view(template_name="core/help.html"),
         name="help"),
    path("settings/", generic.TemplateView.as_view(template_name="core/settings.html"),
         name="settings")
]
