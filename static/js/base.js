$(document).ready(function () {

    // SIDENAV INITIALIZATION
    $('.sidenav').sidenav();

    // DROPDOWN INITIALIZATION
    $('.dropdown-trigger').dropdown({
            autoTrigger: false,
            coverTrigger: false,
            inDuration: 300,
            outDuration: 400,
            gutter: 0
        }
    );

    // MODAL INITIALIZATION
    $('.modal').modal();

    // SELECT INITIALIZATION
    $('select').formSelect();

    // DATERANGEPICKER INITIALIZATION
    $('input[name="dates"]').daterangepicker({
        opens: 'left',
        minYear: 2020,
        autoApply: true,
        autoUpdateInput: true,
    });

    // DISABLE BUTTON ON RESET PASSWORD FORM
    $('.password-reset-button').attr("disabled", "disabled");
    $('input[type="email"]').on("keyup", function () {
        if ($('#id_email').val() !== "") {
            $('.password-reset-button').removeAttr("disabled");
        } else {
            $('.password-reset-button').attr("disabled", "disabled");
        }
    });

    // SHOW AND HIDE NOTIFICATIONS PANEL
    $('#notifications').click(function () {
        $('#notifications-container').css({"display": 'block'})
    });
    $('main').click(function () {
        $('#notifications-container').css({"display": 'none'})
    });
});