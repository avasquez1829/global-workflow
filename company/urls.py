from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .api.views import CompanyViewSet
from .api import views

router = DefaultRouter()
router.register('', CompanyViewSet, basename='company')

urlpatterns = router.urls
# urlpatterns = [
#     path('company_list/', views.company_data_list, name='company_list'),
#     path('<int:pk>/', views.company_data_detail, name="company_detail"),
# ]
