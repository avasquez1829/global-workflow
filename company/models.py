from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from bpmn_project.settings import AUTH_USER_MODEL


# Create your models here.
class Company(models.Model):
    """Model definition for Company."""
    name = models.CharField(_("Name"), max_length=100)
    branch = models.CharField(_("branch"), max_length=50)
    logo = models.ImageField(_("Company logo"), upload_to="company_logo",
                             blank=True)

    class Meta:
        """Meta definition for Company."""

        verbose_name = "Company"
        verbose_name_plural = "Company's"

    def __str__(self):
        """Unicode representation of Company."""
        return self.branch


class Department(models.Model):
    """Model definition for Department."""

    name = models.CharField(_("department"), max_length=40)

    class Meta:
        """Meta definition for Department."""

        verbose_name = "Department"
        verbose_name_plural = "Departments"

    def __str__(self):
        """Unicode representation of Department."""
        return self.name


class Position(models.Model):
    """Model definition for Position."""

    company = models.ManyToManyField(Company)
    name = models.CharField(_("name"), max_length=50)

    class Meta:
        """Meta definition for Position."""

        verbose_name = "Position"
        verbose_name_plural = "Positions"

    def __str__(self):
        """Unicode representation of Position."""
        return self.name


class PositionDepartment(models.Model):
    """Model definition for PositionDepartment."""

    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for PositionDepartment."""

        verbose_name = "PositionDepartment"
        verbose_name_plural = "PositionDepartments"

    def __str__(self):
        """Unicode representation of PositionDepartment."""
        return f"{self.position} in {self.department}"


class OrganizationHierarchy(models.Model):
    """Model definition for OrganizationHierarchy."""

    position_department = models.ForeignKey(
            PositionDepartment, on_delete=models.CASCADE
    )
    top_organization_hierarchy = models.PositiveIntegerField()

    class Meta:
        """Meta definition for OrganizationHierarchy."""

        verbose_name = "OrganizationHierarchy"
        verbose_name_plural = "OrganizationHierarchy's"

    def __str__(self):
        """Unicode representation of OrganizationHierarchy."""
        pass


class UserOrganizationHierarchy(models.Model):
    """Model definition for UserOrganizationHierarchy."""

    user = models.ForeignKey(
            settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    organization_hierarchy = models.ForeignKey(
            OrganizationHierarchy, on_delete=models.CASCADE
    )

    class Meta:
        """Meta definition for UserOrganizationHierarchy."""

        verbose_name = "UserOrganizationHierarchy"
        verbose_name_plural = "UserOrganizationHierarchy's"

    def __str__(self):
        """Unicode representation of UserOrganizationHierarchy."""
        return f"{self.user} in organization hierarchy"
