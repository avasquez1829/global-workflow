from rest_framework import viewsets
from ..models import Company
from .serializers import CompanySerializer
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework.permissions import IsAdminUser


class CompanyViewSet(viewsets.ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()
    permission_classes = [IsAdminUser]


company_data_list = CompanyViewSet.as_view({'get': 'list', 'post': 'create'})
company_data_detail = CompanyViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'path': 'partial_update',
         'delete': 'destroy'})
